# Symfony Vagrant Box

Basic Ubuntu 14.04 vagrant box with:

- PHP 5.5
- Apache 2.4
- MySql 5.5

And:

- Java 1.7.0_55
- Curl
- Git
- Composer
- PHPMyAdmin
- Vim
- Node.js
- Less
- Sass (with compass)
- ACL

## Customizations

At first run vagrant enables apache modules:
authn_core - required by PHPMyAdmin
access_compat - for compatibility with Apache2.2 vhosts/.htaccess configs


## Requirements
1. [VirtualBox](https://www.virtualbox.org/) 4.3.10 >=
2. [Vagrant](http://www.vagrantup.com/) 1.5.3 >=
3. [Git](http://git-scm.com/)
If you want to use rsync (and you should)
4.  Cygwin, and by cygwin
	4a: rsync
	4b: openssl


### Installation
1. Clone this repository.
2. Run git submodule update --init
3. Run `vagrant up` in the newly created directory. 
4. Add `192.168.56.101 local.dev` and  `192.168.56.101 vagrant` to your HOSTS file.
5. Clone your project into  `/var/www/project/`
6. Turn on ACL - read about it in section "Enabling ACL"
7. run `sudo SymfonyCorrectRights /var/www/project` to set folders permissions correctly. (this requires ACL, remember to turn this on - you can read about it few lines below).
8. Modify `/var/www/project/web/app_dev.php` and add `192.168.56.1` to the local ip security array or comment out the section entirely.
9. You should now be able to see http://local.dev/app_dev.php/ in your browser.


###
By default this repository is set up to use rsync.
Check Vagrant file for another options:
1. Default (line 18)
2. OSX best solution (line 21)
3. Rsync (starting from line 26)
Comment/uncomment what's best for you.
I've checked only native and rsync-auto syncing modes on Windows. Native is not working correctly with cache folder permissions, rsync works as it should.
For other systems you have to check it by yourself.
I'll be checking also an SMB option soon. Will update this docs.

### Increasing Speed with Windows Hosts
Currently using rsync on Windows hosts is the best way to overcome the slow Virtual Box synced folders. You can either use `vagrant rsync-auto` to automatically push your changes to your box or your can utilize your IDE's deployment tools to automatically upload your changes. 

I've found PHPStorm works well once you've configured the deployment settings correctly. The only minor annoyance is the need to do a `vagrant rsync` after switching branches.
**Tip:** Clicking the "Remote Host" button will keep your SSH connection alive and greatly improve the transfer time.

To setup rsync on a Windows host you'll need to install using cygwin:

1. Download and install [cygwin](http://www.cygwin.com/). 
2. Add the rsync package and the openssl package.
3. Add the cygwin bin directory (C:\cygwin64\bin) to your local path variable.

Your next `vagrant up` will automatically rsync your project directory to /var/www/project.
Currently, Vagrant has problems with enabling rsync after you close host system without suspending vagrant.
If your files do not sync, run `vagrant rsync-auto` in your vagrant dir to turn vagrant sync on.

###Enabling ACL

To set up app/cache & app/logs files permissions you need to have ACL turned on. 
Currently this works only when RSYNC is enabled, as on other modes sync folder is mounted on the start, and it's not possible to enable ACL on such disk.

Edit as sudo `/etc/fstab` on guest machine and add "acl" option to drive mounted as `/` - complete options should be set to `errors=remount-ro,acl`
Run 'sudo mount -o remount /'.

Set your permissions to Sf2 files by running:
```bash
sudo rm -rf app/cache/*

sudo chmod 777 -Rf app/cache/*
sudo chmod 777 -Rf app/logs/*

sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs
sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx app/cache app/logs

app/console cache:clear --no-warmup
```

Machine should also have a script called "SymfonyCorrectRights" in bin folder, so it's enought to run `sudo SymfonyCorrectRights .` in your project dir.



#### Increasing Speed on Windows Hosts using Virtual Box Default Synced Folders
If you still want to use the native Virtual box synced folders you can move the cache to the /tmp/ folder within the guest machine by adding these two functions in the AppKernel.php to the AppKernel class:

```php
public function getCacheDir()
{
    return '/tmp/symfony/cache/'. $this->environment;
}

public function getLogDir()
{
    return '/tmp/symfony/log/'. $this->environment;
}
```


### SSH
 You can access vagrant by `ssh vagrant@local.dev -i puphpet/files/dot/ssh/id_rsa` called in your vagrant dir. Add your own key to `.ssh/known_hosts`:)

### Default MySQL Password
- Username: devel
- Password: devel

PHPMyAdmin is at local.dev/phpmyadmin.
Adminier is at local.dev/adminer


### Default Apache Virtual Host
- ServerName: local.dev
- ServerAlias: www.local.dev
- DocumentRoot: /var/www/project/web

### A Note About the Private Network
This vagrant box is configured on a private network at 192.168.56.101 which is accessible from the guest OS. You can connect a GUI MySQL client by using the default port (3306) and a host ip of 192.168.56.101. See the [Vagrant documentation](http://docs.vagrantup.com/v2/networking/private_network.html) for additional information.

